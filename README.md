# Fractal_Designer
This is an application to make fractal videos based on Qt(6.0.0), C++ and FFmpeg.

## Version Information
This is version 5.5.3 updated in 2021/04/13.
There are release versions for Windows and Linux.
The code is known to work properly in Win 10 and Ubuntu 20.
- For Windows, just run the exe file
- For Linux, run Fractal_Designer.sh

## Main Features
- You can create fractal images and videos at your command, with the freedom of designing colours and routes.
- Equipped with the Route Tool, it is easy to find an appropriate route.
- More functions are there to be discovered by you! For more information, you can find them in help.

## Tips
- Try not to use the compatibility mode with Fractal Designer 4 as it is error-prone.
- In this version, Fractal Designer supports 4 templates, two of which require additional settings.
- You can report bugs if any.

## Username and Passcode
You can use any of them to log in the application:

|Username|Passcode|
|-|-|
|CSDN|Southeast61|
|GitHub|Southeast61|
|SEU|SEU615205|

## Download Release
Now there is only release versions for Windows and Linux.
